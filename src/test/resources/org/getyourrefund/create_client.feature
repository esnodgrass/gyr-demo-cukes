Feature: Create a client
  Scenario: Add client to existing organization
    Given a client is using GetYourRefund.org
    And they selected 2020 for their tax year
    And they start the demo
    And they decide to actually get started
    And they continue on from the overview
    And they have provided basic information
    And they confirmed vita site
    And they have provided a phone number
    And they have provided an email address
    And they have selected email as notification preference
    And the pertinent information is provided
    And they confirmed no tax credits
    And they confirmed no identity theft
    And they confirm they are not legally married
    And they do not want to claim anyone
    And they did not pay child care expenses
    And they did not adopt a child
    And they were not in college or other post high school
    And they had no student loan interest
    And they had 1 job
    And they did not work in other states
    And they worked for someone else
    And there were no disability benefits
    And they had no interest from dividends
    And they sold no stocks, bonds, or real estate
    And they had no SSI
    And they had no other money
    And they did not purchase insurance through marketplace
    And they had no HSA
    And they had no medical expenses
    And they had no charitable expenses
    And they had no gambling income
    And they had no school supplies expenses
    And they had no other tax expenses
    And they had a state refund last year
    And they did not sell a home
    And they did not pay mortgage interest
    And they did not receive homebuyer credit in 2008
    And they did not have a federal disaster cost
    And they had no debt cancellation
    And they had no letter or bill from IRS
    And they had not been disallowed the EITC
    And they did not apply 2019 refund to 2020
    And no previous year business loss
    And no energy efficient home items
    And they have no further notes to share
    And they confirm they have all their documents
    And they continue to basic information
    And they attach a photo of their id card
    And they move onto submitting a photo holding id
    And they submit photo holding id card
    And they submit SSN or ITIN card
    And they move onto submitting tax documents
    And they upload W2
    And they do not have prior year tax return
    And they confirm all documents are shared
    And they have no meeting preferences
    And they choose direct deposit
    And they choose no savings options
    And the decline to make payment from bank account
    And they have provided bank details
    And they have provided their address
    And they skip the end questions
    When they submit their questionnaire
    Then they should be on the success page
    And they should be provided a confirmation number