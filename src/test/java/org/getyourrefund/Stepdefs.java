package org.getyourrefund;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.Random;
import java.util.UUID;

import static org.junit.Assert.*;

public class Stepdefs {

    private WebDriver driver;
    private int randomNumber = new Random().nextInt(1000);

    @Before
    public void beforeTests() {
        WebDriverManager.chromedriver().setup();
        System.out.println(randomNumber);
    }

    @After
    public void closeBrowser() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Given("a client is using GetYourRefund.org")
    public void aClientIsUsingGetYourRefundOrg() {
        driver = new ChromeDriver();
//        driver.get("https://demo.getyourrefund.org/ocncgov-site");
        driver.get("https://demo.getyourrefund.org/gyr-demo");
        WebElement getStarted = driver.findElement(By.id("firstCta"));
        getStarted.click();
        WebElement fileTaxesLink = driver.findElement(By.xpath("//a[contains(@href,'/en/questions/file-with-help')]"));
        fileTaxesLink.click();
        WebElement continueToFiling = driver.findElement(By.xpath("//a[contains(@href,'/en/questions/backtaxes')]"));
        continueToFiling.click();
    }

    @And("they selected {int} for their tax year")
    public void theyVeSelectedForTheirTaxYear(int taxYear) {
        WebElement taxYearBox = driver.findElement(By.id("backtaxes_form_needs_help_" + taxYear));
        taxYearBox.click();
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        continueButton.click();
    }

    @And("they start the demo")
    public void theyStartTheDemo() {
        WebElement startButton = driver.findElement(By.xpath("//a[contains(@href,'/en/questions/start-with-current-year')]"));
        startButton.click();
    }

    @And("they decide to actually get started")
    public void theyDecideToActuallyGetStarted() {
        WebElement continueToDemo = driver.findElement(By.xpath("//a[contains(@href,'/en/questions/eligibility')]"));
        continueToDemo.click();
        WebElement noneCheckbox = driver.findElement(By.id("none__checkbox"));
        noneCheckbox.click();
        WebElement confirmSituation = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        confirmSituation.click();
    }

    @And("they continue on from the overview")
    public void theyContinueOnFromTheOverview() {
        WebElement continueOnFromOverview = driver.findElement(By.xpath("//a[contains(@href,'/en/questions/personal-info')]"));
        continueOnFromOverview.click();
    }

    @And("they have provided basic information")
    public void theyHaveProvidedBasicInformation() {
        WebElement preferredNameInput = driver.findElement(By.id("personal_info_form_preferred_name"));
        preferredNameInput.sendKeys("Test_random_" + randomNumber);
        WebElement zipCode = driver.findElement(By.id("personal_info_form_zip_code"));
        zipCode.sendKeys("12345");

        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        continueButton.submit();
    }

    @And("they confirmed vita site")
    public void theyConfirmedVitaSite() {
        WebElement confirmVitaSite = driver.findElement(By.xpath("//a[contains(@href,'/en/questions/phone-number')]"));
        confirmVitaSite.click();
    }

    @And("they have provided a phone number")
    public void theyHaveProvidedAPhoneNumber() {
        WebElement phoneNumberInput = driver.findElement(By.id("phone_number_form_phone_number"));
        phoneNumberInput.sendKeys("555 123-4567");
        WebElement confirmPhoneNumberInput = driver.findElement(By.id("phone_number_form_phone_number_confirmation"));
        confirmPhoneNumberInput.sendKeys("555 123-4567");
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        continueButton.click();
    }

    @And("they have provided an email address")
    public void theyHaveProvidedAnEmailAddress() {
        UUID uuid = UUID.randomUUID();

        WebElement emailInput = driver.findElement(By.id("email_address_form_email_address"));
        emailInput.sendKeys(uuid.toString() + "@sharklasers.com");
        WebElement confirmEmailInput = driver.findElement(By.id("email_address_form_email_address_confirmation"));
        confirmEmailInput.sendKeys(uuid.toString() + "@sharklasers.com");
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        continueButton.click();
    }

    @And("they have selected email as notification preference")
    public void theyHaveSelectedEmailAsNotificationPreference() {
        WebElement emailCheckbox = driver.findElement(By.id("notification_preference_form_email_notification_opt_in"));
        emailCheckbox.click();
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        continueButton.click();
    }

    @And("the pertinent information is provided")
    public void thePertinentInformationIsProvided() {
        WebElement firstNameInput = driver.findElement(By.id("consent_form_primary_first_name"));
        firstNameInput.sendKeys("Test");
        WebElement lastNameInput = driver.findElement(By.id("consent_form_primary_last_name"));
        lastNameInput.sendKeys("Testerson");
        WebElement lastFourInput = driver.findElement(By.id("consent_form_primary_last_four_ssn"));
        lastFourInput.sendKeys("1234");

        Select monthOfBirthDropdown = new Select(driver.findElement(By.id("consent_form_birth_date_month")));
        monthOfBirthDropdown.selectByValue("1");

        Select dayOfBirthDropdown = new Select(driver.findElement(By.id("consent_form_birth_date_day")));
        dayOfBirthDropdown.selectByValue("1");

        Select yearOfBirthDropdown = new Select(driver.findElement(By.id("consent_form_birth_date_year")));
        yearOfBirthDropdown.selectByValue("2000");

        WebElement agreeButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        agreeButton.click();
    }

    @And("they confirmed no tax credits")
    public void theyConfirmedNoTaxCredits() {
        WebElement noneCheckbox = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/div/fieldset[5]/label"));
        noneCheckbox.click();
        WebElement confirmSituation = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        confirmSituation.click();
    }


    @And("they confirmed no identity theft")
    public void theyConfirmedNoIdentityTheft() {
        WebElement confirmNoIdentityTheft = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        confirmNoIdentityTheft.click();
    }

    @And("they confirm they are not legally married")
    public void theyConfirmTheyAreNotLegallyMarried() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they do not want to claim anyone")
    public void theyDoNotWantToClaimAnyone() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they did not pay child care expenses")
    public void theyDidNotPayChildCareExpenses() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they did not adopt a child")
    public void theyDidNotAdoptAChild() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they were not in college or other post high school")
    public void theyWereNotInCollegeOrOtherPostHighSchool() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no student loan interest")
    public void theyHadNoStudentLoanInterest() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had {int} job")
    public void theyHadJob(int numberOfJobs) {
        Select numberOfJobsDropdown = new Select(driver.findElement(By.id("job_count_form_job_count")));
        numberOfJobsDropdown.selectByValue(String.valueOf(numberOfJobs));
        WebElement nextButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        nextButton.click();
    }

    @And("they did not work in other states")
    public void theyDidNotWorkInOtherStates() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they worked for someone else")
    public void theyWorkedForSomeoneElse() {
        WebElement someoneElse = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/div/fieldset[1]/label"));
        someoneElse.click();
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        continueButton.click();
    }

    @And("there were no disability benefits")
    public void thereWereNoDisabilityBenefits() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no interest from dividends")
    public void theyHadNoInterestFromDividends() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they sold no stocks, bonds, or real estate")
    public void theySoldNoStocksBondsOrRealEstate() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no SSI")
    public void theyHadNoSSI() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no other money")
    public void theyHadNoOtherMoney() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they did not purchase insurance through marketplace")
    public void theyDidNotPurchaseInsuranceThroughMarketplace() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no HSA")
    public void theyHadNoHSA() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no medical expenses")
    public void theyHadNoMedicalExpenses() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no charitable expenses")
    public void theyHadNoCharitableExpenses() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no gambling income")
    public void theyHadNoGamblingIncome() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no school supplies expenses")
    public void theyHadNoSchoolSuppliesExpenses() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no other tax expenses")
    public void theyHadNoOtherTaxExpenses() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had a state refund last year")
    public void theyHadAStateRefundLastYear() {
        WebElement confirmNotClaiming = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[1]"));
        confirmNotClaiming.click();
    }

    @And("they did not sell a home")
    public void theyDidNotSellAHome() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they did not pay mortgage interest")
    public void theyDidNotPayMortgageInterest() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they did not receive homebuyer credit in 2008")
    public void theyDidNotReceiveHomebuyerCreditIn() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they did not have a federal disaster cost")
    public void theyDidNotHaveAFederalDisasterCost() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no debt cancellation")
    public void theyHadNoDebtCancellation() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had no letter or bill from IRS")
    public void theyHadNoLetterOrBillFromIRS() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they had not been disallowed the EITC")
    public void theyHadNotBeenDisallowedTheEITC() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they did not apply 2019 refund to 2020")
    public void theyDidNotApplyRefundTo() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("no previous year business loss")
    public void noPreviousYearBusinessLoss() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("no energy efficient home items")
    public void noEnergyEfficientHomeItems() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they have no further notes to share")
    public void theyHaveNoFurtherNotesToShare() {
        WebElement nextButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        nextButton.click();
    }

    @And("they confirm they have all their documents")
    public void theyConfirmTheyHaveAllTheirDocuments() {
        WebElement continueButton = driver.findElement(By.xpath("//a[contains(@href,'/en/documents/id-guidance')]"));
        continueButton.click();
    }

    @And("they continue to basic information")
    public void theyContinueToBasicInformation() {
        WebElement continueButton = driver.findElement(By.xpath("//a[contains(@href,'/en/documents/ids')]"));
        continueButton.click();
    }

    @And("they attach a photo of their id card")
    public void theyAttachAPhotoOfTheirIdCard() {
        uploadDocument("fakeid.jpeg", "/en/documents/selfie-instructions");
    }

    @And("they move onto submitting a photo holding id")
    public void theyMoveOntoSubmittingAPhotoHoldingId() {
        WebElement moveToSubmitPhotoButton = driver.findElement(By.xpath("//a[contains(@href,'/en/documents/selfies')]"));
        moveToSubmitPhotoButton.click();
    }

    @And("they submit photo holding id card")
    public void theySubmitPhotoHoldingIdCard() {
        uploadDocument("selfie.png", "/en/documents/ssn-itins");
    }

    private void uploadDocument(String fileName, String continueButtonHref) {
        String filePath = System.getProperty("user.dir") + "/assets/" + fileName;
        driver.findElement(By.id("document_type_upload_form_document")).sendKeys(filePath);
        WebElement continueButton = driver.findElement(By.xpath("//a[contains(@href,'" + continueButtonHref + "')]"));
        continueButton.click();
    }

    @And("they submit SSN or ITIN card")
    public void theySubmitSSNOrITINCard() {
        uploadDocument("SS Cards.png", "/en/documents/intro");
    }

    @And("they move onto submitting tax documents")
    public void theyMoveOntoSubmittingTaxDocuments() {
        WebElement moveToSubmitTaxDocsButton = driver.findElement(By.xpath("//a[contains(@href,'/en/documents/employment')]"));
        moveToSubmitTaxDocsButton.click();
    }


    @And("they upload W2")
    public void theyUploadW2() {
        uploadDocument("w2.png", "/en/documents/additional-documents");
    }

    @And("they do not have prior year tax return")
    public void theyDoNotHavePriorYearTaxReturn() {
        WebElement moveToSubmitTaxDocsButton = driver.findElement(By.xpath("//a[contains(@href,'/en/documents/overview')]"));
        moveToSubmitTaxDocsButton.click();
    }

    @And("they confirm all documents are shared")
    public void theyConfirmAllDocumentsAreShared() {
        WebElement allDocsSharedButton = driver.findElement(By.xpath("//a[contains(@href,'/en/questions/interview-scheduling')]"));
        allDocsSharedButton.click();
    }

    @And("they have no meeting preferences")
    public void theyHaveNoMeetingPreferences() {
        WebElement confirmMeetingPreferences = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        confirmMeetingPreferences.click();
    }


    @And("they choose direct deposit")
    public void theyChooseDirectDeposit() {
        WebElement directDeposit = driver.findElement(By.id("refund_payment_form_refund_payment_method_direct_deposit"));
        directDeposit.click();
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        continueButton.click();
    }

    @And("they choose no savings options")
    public void theyChooseNoSavingsOptions() {
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        continueButton.click();
    }

    @And("the decline to make payment from bank account")
    public void theDeclineToMakePaymentFromBankAccount() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/div/main/form/div[2]/button[2]"));
        noButton.click();
    }

    @And("they have provided bank details")
    public void theyHaveProvidedBankDetails() {
        WebElement bankName = driver.findElement(By.id("bank_details_form_bank_name"));
        bankName.sendKeys("First State Bank of the State");
        WebElement routingNumber = driver.findElement(By.id("bank_details_form_bank_routing_number"));
        routingNumber.sendKeys("123456789");
        WebElement accountNumber = driver.findElement(By.id("bank_details_form_bank_account_number"));
        accountNumber.sendKeys("098765432101");

        WebElement checkingAccountOption = driver.findElement(By.id("bank_details_form_bank_account_type_checking"));
        checkingAccountOption.click();

        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        continueButton.click();
    }

    @And("they have provided their address")
    public void theyHaveProvidedTheirAddress() {
        WebElement streetAddress = driver.findElement(By.id("mailing_address_form_street_address"));
        streetAddress.sendKeys("1 W Campbell Rd");
        WebElement city = driver.findElement(By.id("mailing_address_form_city"));
        city.sendKeys("Schenectady");
        WebElement continueButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        continueButton.click();
    }

    @And("they skip the end questions")
    public void theySkipTheEndQuestions() {
        WebElement noButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button[2]"));
        noButton.click();
    }

    @When("they submit their questionnaire")
    public void theySubmitTheirQuestionnaire() {
        WebElement submitButton = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/form/button"));
        submitButton.click();
    }

    @Then("they should be on the success page")
    public void theyShouldBeOnTheSuccessPage() {
        String currentURL = driver.getCurrentUrl();
        assertTrue(currentURL.contains("/successfully-submitted"));
    }

    @And("they should be provided a confirmation number")
    public void theyShouldBeProvidedAConfirmationNumber() {
        WebElement confirmation = driver.findElement(By.xpath("/html/body/div[1]/section/div/div/main/h2"));
        assertTrue(confirmation.getText().contains("Your confirmation number is:"));
    }
}
